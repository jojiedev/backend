# Backend Project

This project contains the backend code for Carconnect Coding Challenge.

## Setup Instructions

Follow these steps to set up and run the backend project:

1. Clone the repository:
    git clone git@gitlab.com:jojiedev/backend.git

2. Navigate to the project directory:
    cd backend

3. Install dependencies:
    npm install

4. Start the development server:
    npm run dev

## Node.js Version Requirement

This project requires Node.js version 16 or higher. Ensure that you have Node.js installed on your system with the correct version before running the project.

## Database Migration

To perform database migration, follow these steps:

1. Create a database with the name "customer_app".

2. Run the migration script:

    npm run db:migrate

## Sample Payload

Use the following sample payload to test the API:

```json
{
"firstName": "Jame",
"lastName": "Tan",
"email": "tan.doe@example.com",
"phoneNumber": "0412345678",
"postcode": "12345"
}
