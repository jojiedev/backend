import express, { Express, Request, Response } from "express";
import dotenv from "dotenv";
import userRoutes from './api/userRoutes'; 
import bodyParser from 'body-parser';
const cors = require('cors');


dotenv.config();

const app: Express = express();

app.use(cors());

// Parse JSON bodies
app.use(bodyParser.json());

// Parse URL-encoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

const port = process.env.PORT || 3000;

app.get("/", (req: Request, res: Response) => {
  res.send("Express + TypeScript Server");
});

// Use the user routes
app.use('/api/v1', userRoutes);

app.listen(port, () => {
  console.log(`[server]: Server is running at http://localhost:${port}`);
});