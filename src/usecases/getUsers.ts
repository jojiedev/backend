const db = require('../models/index');

const User = db.User;
export async function getAllUsers() {
    try {
      const users = await User.findAll();
      return users;
    } catch (error) {
      throw new Error('Unable to retrieve users');
    }
  }
  