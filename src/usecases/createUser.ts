const db = require('../models/index');

const User = db.User;

interface CreateUserInput {
  firstName: string;
  lastName: string;
  email: string;
  phoneNumber: string;
  postcode: string;
}

export async function createUser(userData: CreateUserInput) {
  try {
    const existingUser = await User.findOne({ where: { email: userData.email } });
    if (existingUser) {
      throw new Error('Email already exists');
    }
    const user = await User.create(userData);
    return user;
  } catch (error) {
    throw new Error('Unable to create user');
  }
}

