const { Model, DataTypes } = require('sequelize');

class User extends Model {
  // Static methods, associations, etc., can be defined here
  static associate(models) {
    // Define associations here
    // Example: this.hasMany(models.OtherModel, { foreignKey: 'userId', as: 'otherModels' });
  }
}

module.exports = (sequelize) => {
  User.init({
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phoneNumber: {
      type: DataTypes.STRING,
      // Note: The original TypeScript had this field as non-nullable.
      // Adjust allowNull based on your actual requirements.
      allowNull: true,
    },
    postCode: {
      type: DataTypes.STRING,
      // Note: The original TypeScript had this field as non-nullable.
      // Adjust allowNull based on your actual requirements.
      allowNull: true,
    },
  }, {
    sequelize,
    modelName: 'User',
    tableName: 'users',
    // Other model options go here
  });

  return User;
};
