import { Request, Response } from 'express';
import { createUser } from '../usecases/createUser'; // Adjust the import path as needed
import { getAllUsers as fetchAllUsers } from '../usecases/getUsers';

export async function postUser(req: Request, res: Response) {
  try {
    const user = await createUser(req.body);
    res.status(201).json(user);
  } catch (error) {
    res.status(400).json({ message: 'Unable to create user'});
  }
}

export async function getAllUsers(req: Request, res: Response) {
  try {
    const users = await fetchAllUsers();
    res.json(users);
  } catch (error) {
    console.error('Error fetching users:', error);
    res.status(500).json({ message: 'Unable to retrieve users' });
  }
}

